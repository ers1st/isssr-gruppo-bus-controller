package hello;

public class Greeting {
	
	private final String tag;
	private final String data;
	private final String id;
	private final long version;
	private final String object;
	private final String project;
	private final String destinationAdress;
	
	public Greeting(String id, String data) {
		
		this.tag = "livel3Direct";
		this.data = data;
		this.id = id;
		this.version = 1;
		this.object = "";
		this.project = "";
		this.destinationAdress = "";
	}
	
	public String getTag() {
		return tag;
	}
	public String getData() {
		return data;
	}
	public String getId() {
		return id;
	}
	public long getVersion() {
		return version;
	}
	public String getObject() {
		return object;
	}
	public String getProject() {
		return project;
	}
	public String getDestinationAdress() {
		return destinationAdress;
	}

}
