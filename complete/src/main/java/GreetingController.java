package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	private final String json = "{\"id\":\"1\",\"name\":\"John Doe\", \"message\": \"Greetings\"}";
    private final String id = "38400000-8cf0-11bd-b23e-10b96e4ef00d";

    @RequestMapping("/greeting")
    public Greeting greeting(String name) {
    	
    	return new Greeting(id, json);
    	
    }
}
